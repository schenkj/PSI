import socket

def Main():
	localHost = '127.0.0.1'
	port = 1112

	s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	s.connect((localHost,port))

	while True:
		s.send(input('\nEnter your message:').encode('ascii'))
		data = s.recv(1024)
		print('Received from the server :',str(data.decode('ascii')))

		
	s.close()

if __name__ == '__main__':
	Main()
