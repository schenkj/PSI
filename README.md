# 2. Úkol – Vícevláknový TCP server
## Vícevláknový HTTP/TCP server
Pro implementaci této úlohy jsem si zvolil jako programovací jazyk python na doporučení kolegy. Přijde mi, že python práci značně zjednudušuje v porovnání s C ve kterém jsem dělal předchozí úlohu.
Protože neumím dost dobře s dockerem, scripty z tohoto cvičení nejsou spouštěny pomocí docker containers, ale přímo jako scripty z příkazové řádky. Uživatel tedy potřebuje mít nainstalovaný python.

Program je ukázkou jednoduché implementace multithread TPC serveru a HTTP klienta, pro procvičení znalostí z předmětu KIV/PSI.

## Spuštění
Spouštění probíhá přes command line. Pro spuštění serveru lze použít:
```sh
python server.py
```
, případně:
```sh
python3 server.py
```
Client je pak spouštěn z nové command line pomocí příkazu:
```sh
python client.py
```
, případně:
```sh
python client.py
```

## Závěr
Doufám, že příliš nevadí, že jsem si zjednodušil práci tím, že jsem si vybral python jakožto implementační jazyk. 
Věřím ale, že práce splňuje požadavky a vyzkoušel jsem si na ní vše co měla práce obsahovat.
