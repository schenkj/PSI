import socket
from _thread import *
import threading
 
print_lock = threading.Lock()
 
def threaded(c):
    while True:
        data = c.recv(1024)
        if not data:
            print('Disconnecting')
            break
        print(data)
        c.send(data)
 
    c.close()
 
 
def Main():
    host = ""

    port = 1112
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)       #socket
    s.bind((host, port))                                        #bind
    print("socket binded -", port)
    s.listen(5)                                                 #listen
    print("listening")
 
    while True:
        c, addr = s.accept()                                    #accept
        print('Connected to :', addr[0], ':', addr[1])
        x = threading.Thread(target=threaded, args=(c,))
        x.start()
        print ('thread started')
    s.close()
 
 
if __name__ == '__main__':
    Main()
